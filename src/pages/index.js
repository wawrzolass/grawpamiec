import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

//JSX  połaczenie html i JS - bo to co niżej to już nie czysty html

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Witajcie ludzie!</h1>
    <p>Welcome to your new Gatsby site.</p>
    <p>Now go build something great.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/moja-strona/">Go to Moja strona</Link> <br />
    <Link to="/kolkoikrzyzyk/">Zagraj w kółko i krzyżyk</Link> <br />
    <Link to="/memory/">Zagraj w Memory</Link> <br />
    <Link to="/using-typescript/">Go to "Using TypeScript"</Link>
  </Layout>
)

export default IndexPage
