import { Link } from 'gatsby';
import React from 'react';  
import Layout from '../components/layout';
import SEO from '../components/seo';
import CircleAndCrossGame from '../components/CrossAndCircleGame';

const KolkoiKrzyzykStrona = () => {
           return (
            <Layout>
                <SEO title="Kółko i krzyżyk" />              
                <h1>Kółko i krzyżyk</h1>
                <Link to="/">Wróć na stronę główną</Link>
                <CircleAndCrossGame />
                
            </Layout>
        );   
}
export default KolkoiKrzyzykStrona;