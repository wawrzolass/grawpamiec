import { Link } from 'gatsby';
import React from 'react';  
import Layout from '../components/layout';
import SEO from '../components/seo';
import MemoryBoard from '../components/memoryGame';

const MemoryGamePage = () => {
           return (
            <Layout>
                <SEO title="Gra w pamięć" />              
                <h1>Gra w pamięć</h1>
                <Link to="/">Wróć na stronę główną</Link>
                <MemoryBoard />
                
            </Layout>
        );   
}
export default MemoryGamePage;