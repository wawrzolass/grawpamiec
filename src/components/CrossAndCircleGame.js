import React, {useState, useEffect} from 'react';
import styled from 'styled-components';

const StyledPole = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 300px;
    margin: 0 auto;
`;
const StyledBaton = styled.button`
    height: 100px;
    width: 100px;
    outline: none;
    text-transform: uppercase;
    font-weight: bold;
    font-size: 110px;
    font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
    background-color: yellow;
`;

const FIELD_VALUES = {
    EMPTY: '',
    CROSS: 'x',
    CIRCLE: 'o'
};

const initialValues = [
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
    FIELD_VALUES.EMPTY,
];

const winningIndexCombinations = [
    [0,1,2],[3,4,5],[6,7,8],
    [0,3,6],[1,4,7],[2,5,8],
    [0,4,8], [2,4,6] 
];

const CircleAndCrossGame = () => {
    const [fields, setFields]= useState(initialValues);
    const [player, setPlayer] = useState(FIELD_VALUES.CROSS);
    const [result, setResult] = useState(null);

    useEffect(() => {
        // metoda sprawdzajaca czy jakis gracz wygral
        const playerWon = checkWinners();
        const anyEmptyFieldLeft = fields.indexOf(FIELD_VALUES.EMPTY) !== -1;
        const isDraw = !playerWon && !anyEmptyFieldLeft ;

        // jesli "x" lub "o" to wygral. Jesli "" to nikt nie wygral
        if(playerWon) {
            setResult(playerWon);
            
        }
        if(isDraw) {
            setResult(FIELD_VALUES.EMPTY);
        }
        
    }, [fields]);
    

    const checkWinners = () => {
        // sprawdzenie czy wygral krzyzyk
        const isCrossWinner = checkIfPlayerWon(FIELD_VALUES.CROSS);
        if(isCrossWinner) {
            return FIELD_VALUES.CROSS;
        }
        // sprawdzenie czy wygralo kolko
        const isCircleWinner = checkIfPlayerWon(FIELD_VALUES.CIRCLE);
        if(isCircleWinner) {
            return FIELD_VALUES.CIRCLE;
        }
        // jesli nikt nie wygral to zwracam ""
        return FIELD_VALUES.EMPTY;
    }
    const checkIfPlayerWon = (checkingPlayer) => {
        // sprawdzenie wszystkich kombinacji na bazie winningIndexCombinations
        // tablica w rezultacie ma postac [false, false, false, false, true itd...] 
        const combinationCheckArray = winningIndexCombinations.map((combination) => {
            return checkIfFieldsAreEqualToPlayer(combination, checkingPlayer);
        });
        // jesli true jest w tablicy combinationCheckArray to cala metoda tez zwraca true 
        return combinationCheckArray.indexOf(true) > -1;
    }
    const checkIfFieldsAreEqualToPlayer = (checkingFields, checkingPlayer) => {
        // sprawdzenie wszystkich pol czy maja wartosc = checkingPlayer
        // tablica ma postac [true, true, true]
        const checkedFields = checkingFields.map(fieldIndex => {
            return fields[fieldIndex] === checkingPlayer;
        });
        // Jesli w tablicy nie ma false (indexOf = -1) to cala metoda zwraca true
        return checkedFields.indexOf(false) === -1;
    }




    // const onClickBoard = () => setPlayer((player) => {
    //     if(player == FIELD_VALUES.CROSS)
    //         return FIELD_VALUES.CIRCLE
    //     else 
    //         return FIELD_VALUES.CROSS;
    // });

    const onClick = (index) => {
        if(fields[index] !== FIELD_VALUES.EMPTY || result !== null){
            return;
        }
        
        // setFields([
        //     ...fields.slice(0,index),
        //     player,
        //     ...fields.slice(index+1)
        // ]);
        
        setFields(fields.map((fieldValue, i) => index === i ? player : fieldValue
            ))

        changePlayer();

    }
    const changePlayer = () => {
        setPlayer(player === FIELD_VALUES.CROSS ? FIELD_VALUES.CIRCLE : FIELD_VALUES.CROSS);
    }

    const klikButon = ()=>{
        setResult(null);
        setFields(initialValues);
    }

    const showResult = result !== null;

    return(<>
        <h1>Tu będzie gra</h1>
        <h2>Teraz kolej: {player}</h2>
        {showResult && (
            <div>
                <h2>{result === '' ? 'Remis' : `Wygrał gracz: ${result}`}</h2>
                <button onClick={klikButon}> Od nowa!</button>
            </div>
        ) }
        
        {/* <h3>Mamy remis!</h3> */}
        <StyledPole >
            {fields.map((fieldValue, index) => (
             <StyledBaton 
             key={index}
             onClick={() => onClick(index)} 
             key={index}>
             {fieldValue}
             </StyledBaton>
             
            ))}
        </StyledPole>
    </>);
}

export default CircleAndCrossGame;