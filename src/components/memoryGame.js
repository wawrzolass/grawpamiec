import React,{useState, useEffect} from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';

import AudioIMG from '../images/audio.png';
import BmpIMG from '../images/bmp.png'; 
import GifIMG from '../images/gif.png';
import HplogoIMG from '../images/hp_logo.png'; 
import JpgIMG from '../images/jpg.png';
import MovieIMG from '../images/movie.png'; 
import MpgIMG from '../images/mpg.png';
import TifIMG from '../images/tif.png'; 

const StyledWrapper = styled.div`
    border: 1px solid red;
    padding: 20px;
    width: 570px;
    display: flex;
    flex-wrap: wrap;
`;
const StyledCard = styled.button`
    border: 1px solid #000;
    height: 130px;
    width: 25%;

    img{
        margin: 0;
        transition: all 0.5s ease;
        opacity: ${({active}) => active? '1': '0'};
        transform: ${({active}) => active? 'scale(1) rotate(360deg)': 'scale(0)'};
        max-width: 100%;
        max-height: 100%;
    }
    

    `;

const imagesDefinitions = [
    {key: 0,
     src: AudioIMG 
     },
     {key: 1,
      src: BmpIMG  },
     {key: 2,
        src: GifIMG },
 {key: 3,
 src: HplogoIMG},
 {key: 4,
 src: JpgIMG  },
 {key: 5,
 src:MovieIMG },
 {key: 6,
src:MpgIMG },
{key: 7,
 src: TifIMG}
];



const MemoryBoard = () => {
    
    const [moveNumber, setMoveNumber] = useState(1);
    const[images, setImages] = useState([]);
    const[clickedImages, setClickedImages] = useState([]);
    const [guessedKeys, setGuessedKeys] = useState([]);

    useEffect(() =>{
        setupImages();
    },[]);

    useEffect(()=>{
        if(clickedImages.length !== 2) {
            return;
        }
        const [firstImageIndex, secondImageIndex] = clickedImages;
        const firstImageKey = images[firstImageIndex].key;
        const secondImageKey = images[secondImageIndex].key;
        let timeoutTime = 1500;
        if(firstImageKey === secondImageKey){
            timeoutTime = 0;
            setGuessedKeys([...guessedKeys, firstImageKey])
        }
        setTimeout(() => {
            setMoveNumber(moveNumber+1);
        setClickedImages([]);
        },timeoutTime);
    },[clickedImages]);

    const setupImages = () => {
        const imagesArray = [...imagesDefinitions, ...imagesDefinitions];
        for(let i=imagesArray.length -1; i>0; i--){
            const j = Math.floor(Math.random() * i)
            const temp = imagesArray[i]
            imagesArray[i] = imagesArray[j]
            imagesArray[j] = temp
        }
        setImages(imagesArray);
    }
      
    const isImageVisible =  (index) => {
        const isImageClicked = clickedImages.indexOf(index) > -1;
        const key = images[index].key;
        //const {key} = images[index];
        const isImageGuessed = guessedKeys.indexOf(key) > -1;
        return isImageClicked || isImageGuessed;
    }

    const onClick = (index) => {
        if(isImageVisible(index) || clickedImages.length > 1){
            return;
        }
        setClickedImages([...clickedImages,index]);
    }

    const onReset = ()=> {
        setupImages();
        setMoveNumber(1);
        setClickedImages([]);
        setGuessedKeys([]);
    }

    const isResetVisible = guessedKeys.length === imagesDefinitions.length;

    return(<>
    <h1>Tu jest gra w pamięć</h1>
    {!isResetVisible &&(<h2>Ruch numer: {moveNumber}</h2>)}
    {isResetVisible && (
    <h3>Gra skończona! <Button variant="contained" color="primary" 
    onClick={onReset}> uruchom ponownie </Button></h3>
    )}
    <StyledWrapper>
    {images.map((image,key) => (
        <StyledCard key={key}  
        active={isImageVisible(key)}
        onClick={() => onClick(key)} >
            <img  src={image.src} />
        </StyledCard>    
    ))}
    
            
            
        </StyledWrapper>
    </>);
}

export default MemoryBoard;